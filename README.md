## Photos

**This application is built using MVVM**

### FUNCTIONALITY

- On launch all the data is fetched using URL Session
- Tapping on a user shows the corresponding albums
- Tapping on an album shows the corresponding photos
- Supports both Landscape and Portrait modes

## TECHNOLOGIES USED

- I did everything in native Swift, used some of my protocols & sub classes from my previous projects

## SCREENSHOTS

![Users](https://bitbucket.org/RushBenazir/photos/raw/fe518f5c36fd9d5dc7e910666cf66599a9bad3d3/Imgs/users.png)
![Albums](https://bitbucket.org/RushBenazir/photos/raw/fe518f5c36fd9d5dc7e910666cf66599a9bad3d3/Imgs/albums.png)
![Photos](https://bitbucket.org/RushBenazir/photos/raw/fe518f5c36fd9d5dc7e910666cf66599a9bad3d3/Imgs/photos.png)