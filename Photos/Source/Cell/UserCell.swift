//
//  UserCell.swift
//  Photos
//
//  Created by RB on 2019-11-16.
//  Copyright © 2019 RB. All rights reserved.
//

import UIKit

class UserCell: UICollectionViewCell, ViewModelBindable {
    
    // MARK: - View model
    
    var viewModel: CellBindable?
    
    func setup(with viewModel: CellBindable) {
        self.viewModel = viewModel
        guard let vm = viewModel as? UserViewModel else { return }
           
        let tapAction = UITapGestureRecognizer(target: vm, action: #selector(vm.didSelect(sender:)))
        contentView.addGestureRecognizer(tapAction)
        
        usernameLabel.text = vm.usernameText
        nameLabel.text = vm.nameText
        addressLabel.text = vm.addressText
        emailLabel.text = vm.emailText
        phoneNumberLabel.text = vm.phoneNumberText
    }
    
    // MARK: - Properties
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var accessoryImageView: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setup()
    }

    // MARK: -
    
    private func setup() {
        setupLabel(usernameLabel, font: UIFont.preferredFont(forTextStyle: .headline), alignment: .center)
        setupLabel(nameLabel)
        setupLabel(emailLabel)
        setupLabel(phoneNumberLabel)
        setupLabel(addressLabel)
    }
    
    private func setupLabel(_ label: UILabel, font: UIFont = UIFont.preferredFont(forTextStyle: .body), alignment: NSTextAlignment = .left) {
        label.font = font
        label.textColor = UIColor.brandTextColor
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.lineBreakMode = .byWordWrapping
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var newFrame = layoutAttributes.frame
        newFrame.size.height = ceil(size.height)
        layoutAttributes.frame = newFrame
        return layoutAttributes
    }
}
