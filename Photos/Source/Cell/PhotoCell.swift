//
//  PhotoCell.swift
//  Photos
//
//  Created by RB on 2019-11-16.
//  Copyright © 2019 RB. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell, ViewModelBindable {
    
    // MARK: - View model
    
    var viewModel: CellBindable?
    
    func setup(with viewModel: CellBindable) {
        self.viewModel = viewModel
        guard let vm = viewModel as? PhotoViewModel else { return }
        
        nameLabel.text = vm.photoNameText
        guard let url = URL(string: vm.imageURLString) else {
            imageView.image = UIImage(named: "liftSciences")!
            return
        }
        imageView.load(url: url)
    }
    // MARK: - Properties
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        setup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imageView.image = nil
    }
    
    // MARK: -
    
    private func setup() {
        imageView.contentMode = .scaleAspectFit
        nameLabel.font = UIFont.preferredFont(forTextStyle: .caption1)
        nameLabel.textColor = UIColor.brandTextColor
        nameLabel.numberOfLines = 0
        nameLabel.textAlignment = .center
        nameLabel.lineBreakMode = .byWordWrapping
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var newFrame = layoutAttributes.frame
        newFrame.size.height = ceil(size.height)
        layoutAttributes.frame = newFrame
        return layoutAttributes
    }
}
