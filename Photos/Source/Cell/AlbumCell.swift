//
//  AlbumCell.swift
//  Photos
//
//  Created by RB on 2019-11-16.
//  Copyright © 2019 RB. All rights reserved.
//

import UIKit

class AlbumCell: UICollectionViewCell, ViewModelBindable {
    
    // MARK: - View model
    
    var viewModel: CellBindable?
    
    func setup(with viewModel: CellBindable) {
        self.viewModel = viewModel
        guard let vm = viewModel as? AlbumViewModel else { return }
        
        let tapAction = UITapGestureRecognizer(target: vm, action: #selector(vm.didSelect(sender:)))
        contentView.addGestureRecognizer(tapAction)
        
        nameLabel.text = vm.nameText
    }
    
    // MARK: - Properties

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var accessoryImageView: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setup()
    }
    
    // MARK: -
    
    private func setup() {
        nameLabel.font = UIFont.preferredFont(forTextStyle: .title2)
        nameLabel.textColor = UIColor.brandTextColor
        nameLabel.numberOfLines = 0
        nameLabel.textAlignment = .center
        nameLabel.lineBreakMode = .byWordWrapping
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var newFrame = layoutAttributes.frame
        newFrame.size.height = ceil(size.height)
        layoutAttributes.frame = newFrame
        return layoutAttributes
    }
}
