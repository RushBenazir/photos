//
//  UserViewController.swift
//  Photos
//
//  Created by RB on 2019-11-16.
//  Copyright © 2019 RB. All rights reserved.
//

import UIKit

protocol UserDelegate: class {
    func didSelectUser(_ user: User)
}

class UserViewController: ViewController {
    
    // MARK: - Properties
    
    private let users: [User]
    private var data: [CellBindable] = []
    private var cellIdentifier = Set<String>()
    
    weak var coordinator: HomeCoordinator?
    weak var delegate: UserDelegate?
    
    // MARK: - Lifecycle
    
    init(users: [User]) {
        self.users = users
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    // MARK: - Actions
    
    @objc func reload() {
        data = getData()
        collectionView.reloadData()
    }
    
    // MARK: - Helper
    
    private func setup() {
        view.backgroundColor = UIColor.brandBackgroundColor
        collectionView.backgroundColor = UIColor.brandBackgroundColor
        collectionView.dataSource = self
        navigationItem.titleView = TitleView()

        view.addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: guide.topAnchor),
            collectionView.leftAnchor.constraint(equalTo: guide.leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: guide.rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: guide.bottomAnchor)
        ])
        
        reload()
    }
    
    private func getData() -> [CellBindable] {
        var data: [CellBindable] = []
        
        users.forEach { user in
            let userViewModel = UserViewModel(user: user, delegate: delegate)
            data.append(userViewModel)
        }
        
        return data
    }
    
    func registerNib(on collectionView: UICollectionView, with viewModel: CellBindable) {
        let id = viewModel.reuseIdentifier!
        if !cellIdentifier.contains(id) {
            collectionView.register(UINib(nibName: id, bundle: nil), forCellWithReuseIdentifier: id)
            cellIdentifier.insert(id)
        }
    }
}

// MARK: - DataSource

extension UserViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let viewModel = data[indexPath.item]
        registerNib(on: collectionView, with: viewModel)
        let id = viewModel.reuseIdentifier
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: id!, for: indexPath) as! ViewModelBindable & UICollectionViewCell
        cell.setup(with: viewModel)
        return cell
    }
}
