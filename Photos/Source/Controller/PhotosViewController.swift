//
//  PhotosViewController.swift
//  Photos
//
//  Created by RB on 2019-11-16.
//  Copyright © 2019 RB. All rights reserved.
//

import UIKit

class PhotosViewController: ViewController {
    
    // MARK: - Properties
    
    var photos: [Photo]? {
        didSet {
            reload()
        }
    }
    
    private var data: [CellBindable] = []
    private var cellIdentifier = Set<String>()
    
    weak var coordinator: HomeCoordinator?
    
    // MARK: - Lifecycle
    
    init(photos: [Photo]?) {
        self.photos = photos
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    // MARK: - Actions
    
    @objc func reload() {
        data = getData()
        collectionView.reloadData()
    }
    
    // MARK: - Helper
    
    private func setup() {
        if let title = title {
            setNavBarTitle(title)
        }
        
        view.backgroundColor = UIColor.brandBackgroundColor
        collectionView.backgroundColor = UIColor.brandBackgroundColor
        collectionView.dataSource = self
        
        view.addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: guide.topAnchor),
            collectionView.leftAnchor.constraint(equalTo: guide.leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: guide.rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: guide.bottomAnchor)
        ])
        
        reload()
    }
    
    private func setNavBarTitle(_ title: String) {
        let label = UILabel()
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textAlignment = .center
        label.textColor = UIColor.brandColor
        label.text = title
        navigationItem.titleView = label
        navigationItem.titleView?.sizeToFit()
    }
    
    private func getData() -> [CellBindable] {
        var data: [CellBindable] = []

        for photo in photos ?? [] {
            let photoViewModel = PhotoViewModel(photo: photo)
            data.append(photoViewModel)
        }

        return data
    }
    
    func registerNib(on collectionView: UICollectionView, with viewModel: CellBindable) {
        let id = viewModel.reuseIdentifier!
        if !cellIdentifier.contains(id) {
            collectionView.register(UINib(nibName: id, bundle: nil), forCellWithReuseIdentifier: id)
            cellIdentifier.insert(id)
        }
    }
}

// MARK: - DataSource

extension PhotosViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let viewModel = data[indexPath.item]
        registerNib(on: collectionView, with: viewModel)
        let id = viewModel.reuseIdentifier
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: id!, for: indexPath) as! ViewModelBindable & UICollectionViewCell
        cell.setup(with: viewModel)
        return cell
    }
}
