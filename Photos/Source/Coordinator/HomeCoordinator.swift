//
//  HomeCoordinator.swift
//  Photos
//
//  Created by RB on 2019-11-16.
//  Copyright © 2019 RB. All rights reserved.
//

import Foundation

class HomeCoordinator: Coordinator {
    
    // MARK: - Properties
    
    private let dispatchGroup = DispatchGroup()
    
    weak var parentCoordinator: Coordinator?
    var childCoordinators = [Coordinator]()
    var navigationController: NavigationController
    
    private var users: [User] = []
    private var albums: [Album] = []
    private var photos: [Photo] = []
    
    private var selectedAlbums: [Album]?
    private var selectedPhotos: [Photo]?
    
    // MARK: - Controllers
    
    private lazy var userViewController: UserViewController = {
        let vc = UserViewController(users: users)
        vc.coordinator = self
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }()
    
    private lazy var albumsViewController: AlbumsViewController = {
        let vc = AlbumsViewController(albums: selectedAlbums)
        vc.coordinator = self
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }()
    
    private lazy var photosViewController: PhotosViewController = {
        let vc = PhotosViewController(photos: selectedPhotos)
        vc.coordinator = self
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }()
    
    // MARK: - Lifecycle
    
    init(navigationController: NavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        users.removeAll()
        albums.removeAll()
        photos.removeAll()
        handleRequest { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.navigationController.pushViewController(strongSelf.userViewController, animated: false)
        }
    }
    
    private func handleRequest(completion: @escaping () -> Void) {
        var requests: [()] = []
        requests += [getUsers(), getAlbums(), getPhotos()]
        requests.forEach { request in
            request
        }
        dispatchGroup.notify(queue: .main) {
            completion()
        }
    }
    
    // MARK: -
    
    private func getUsers() {
        let userAPI = UserAPI(configuration: .default)
        dispatchGroup.enter()
        
        userAPI.request(from: .getUsers) { result in
            switch result {
            case .success(let users):
                self.users = users
                self.dispatchGroup.leave()
            case .failure(let error):
                print("Error: \(error.localizedDescription)")
                self.dispatchGroup.leave()
            }
        }
    }
    
    private func getAlbums() {
        let albumAPI = AlbumAPI(configuration: .default)
        dispatchGroup.enter()
        
        albumAPI.request(from: .getAlbums) { result in
            switch result {
            case .success(let albums):
                self.albums = albums
                self.dispatchGroup.leave()
            case .failure(let error):
                print("Error: \(error.localizedDescription)")
                self.dispatchGroup.leave()
            }
        }
    }
    
    
    private func getPhotos() {
        let photoAPI = PhotoAPI(configuration: .default)
        dispatchGroup.enter()
        
        photoAPI.request(from: .getPhotos) { result in
            switch result {
            case .success(let photos):
                self.photos = photos
                self.dispatchGroup.leave()
            case .failure(let error):
                print("Error: \(error.localizedDescription)")
                self.dispatchGroup.leave()
            }
        }
    }
}

// MARK: - Delegates

extension HomeCoordinator: UserDelegate {
    func didSelectUser(_ user: User) {
        // fetch corresponding album with user ID
        selectedAlbums = albums.filter({ $0.userId == user.id })
        albumsViewController.title = user.username
        navigationController.pushViewController(albumsViewController, animated: true)
    }
}

extension HomeCoordinator: AlbumDelegate  {
    func didSelectAlbum(_ album: Album) {
        selectedPhotos = photos.filter({ $0.albumId == album.id })
        photosViewController.title = album.title
        navigationController.pushViewController(photosViewController, animated: true)
    }
}
