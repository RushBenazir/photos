//
//  Album.swift
//  Photos
//
//  Created by RB on 2019-11-16.
//  Copyright © 2019 RB. All rights reserved.
//

import Foundation

// MARK: - Album
struct Album: Codable {
    let userId, id: Int
    let title: String

    enum CodingKeys: String, CodingKey {
        case userId, id
        case title
    }
}
