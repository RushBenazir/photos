//
//  Photo.swift
//  Photos
//
//  Created by RB on 2019-11-16.
//  Copyright © 2019 RB. All rights reserved.
//

import Foundation

// MARK: - Photo
struct Photo: Codable {
    let albumId, id: Int
    let title: String
    let url, thumbnailUrl: String

    enum CodingKeys: String, CodingKey {
        case albumId, id
        case title, url
        case thumbnailUrl
    }
}
