//
//  AlbumAPI.swift
//  Photos
//
//  Created by RB on 2019-11-16.
//  Copyright © 2019 RB. All rights reserved.
//

import Foundation

enum AlbumTarget {
    case getAlbums
}

extension AlbumTarget: Endpoint {
    var base: String {
        return "https://jsonplaceholder.typicode.com"
    }
    
    var path: String {
        switch self {
        case .getAlbums: return "/albums"
        }
    }
    
    var urlComponents: URLComponents {
        var components = URLComponents(string: base)!
        components.path = path
        return components
    }
    
    var request: URLRequest {
        guard let url = urlComponents.url else { fatalError("must have valid URL") }
        return URLRequest(url: url)
    }
}

class AlbumAPI: API {
    let session: URLSession
    
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
    
    // in the signature of the function in the success case we define the Class type thats is the generic one in the API
    func request(from target: AlbumTarget, completion: @escaping (Result<[Album], APIError>) -> Void) {
        fetch(with: target.request , decode: { json -> [Album]? in
            guard let albums = json as? [Album] else { return  nil }
            return albums
        }, completion: completion)
    }
}
