//
//  PhotoAPI.swift
//  Photos
//
//  Created by RB on 2019-11-16.
//  Copyright © 2019 RB. All rights reserved.
//

import Foundation

enum PhotoTarget {
    case getPhotos
}

extension PhotoTarget: Endpoint {
    var base: String {
        return "https://jsonplaceholder.typicode.com"
    }
    
    var path: String {
        switch self {
        case .getPhotos: return "/photos"
        }
    }
    
    var urlComponents: URLComponents {
        var components = URLComponents(string: base)!
        components.path = path
        return components
    }
    
    var request: URLRequest {
        guard let url = urlComponents.url else { fatalError("must have valid URL") }
        return URLRequest(url: url)
    }
}

class PhotoAPI: API {
    let session: URLSession
    
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
    
    // in the signature of the function in the success case we define the Class type thats is the generic one in the API
    func request(from target: PhotoTarget, completion: @escaping (Result<[Photo], APIError>) -> Void) {
        fetch(with: target.request , decode: { json -> [Photo]? in
            guard let photos = json as? [Photo] else { return  nil }
            return photos
        }, completion: completion)
    }
}
