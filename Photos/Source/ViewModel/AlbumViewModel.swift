//
//  AlbumViewModel.swift
//  Photos
//
//  Created by RB on 2019-11-16.
//  Copyright © 2019 RB. All rights reserved.
//

import UIKit

class AlbumViewModel {
    private let album: Album
    private var delegate: AlbumDelegate?
    
    init(album: Album, delegate: AlbumDelegate?) {
        self.album = album
        self.delegate = delegate
    }
    
    @objc func didSelect(sender: Any) {
        delegate?.didSelectAlbum(album)
    }
    
    var nameText: String {
        return album.title
    }
}

extension AlbumViewModel: CellBindable {
    var reuseIdentifier: String? {
        return "AlbumCell"
    }
    
    var nibNameOrNil: String? {
        return reuseIdentifier
    }
    
    var cellType: UICollectionViewCell.Type? {
        return nil
    }
}
