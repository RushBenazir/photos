//
//  UserViewModel.swift
//  Photos
//
//  Created by RB on 2019-11-16.
//  Copyright © 2019 RB. All rights reserved.
//

import UIKit

class UserViewModel {
    private let user: User
    private var delegate: UserDelegate?
    
    init(user: User, delegate: UserDelegate?) {
        self.user = user
        self.delegate = delegate
    }
    
    @objc func didSelect(sender: Any) {
        delegate?.didSelectUser(user)
    }
    
    var usernameText: String {
        return user.username
    }
    
    var nameText: String {
        return "Name: \(user.name)"
    }
    
    var addressText: String {
        return "Address: \(user.address.street), \(user.address.city) \(user.address.zipcode)"
    }
    
    var emailText: String {
        return "Email: \(user.email)"
    }
    
    var phoneNumberText: String {
        return "Phone Number: \(user.phone)"
    }
}

extension UserViewModel: CellBindable {
    var reuseIdentifier: String? {
        return "UserCell"
    }
    
    var nibNameOrNil: String? {
        return reuseIdentifier
    }
    
    var cellType: UICollectionViewCell.Type? {
        return nil
    }
}
