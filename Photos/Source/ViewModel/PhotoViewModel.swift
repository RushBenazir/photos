//
//  PhotoViewModel.swift
//  Photos
//
//  Created by RB on 2019-11-16.
//  Copyright © 2019 RB. All rights reserved.
//

import UIKit

class PhotoViewModel {
    private let photo: Photo
    
    init(photo: Photo) {
        self.photo = photo
    }
    
    var imageURLString: String {
        return photo.url
    }
    
    var photoNameText: String {
        return photo.title
    }
}

extension PhotoViewModel: CellBindable {
    var reuseIdentifier: String? {
        return "PhotoCell"
    }
    
    var nibNameOrNil: String? {
        return reuseIdentifier
    }
    
    var cellType: UICollectionViewCell.Type? {
        return nil
    }
}
